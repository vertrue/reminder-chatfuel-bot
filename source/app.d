import vibe.utils.dictionarylist;
import std.stdio;
import std.uuid;
import std.string;
import std.conv;
import vibe.http.server;
import vibe.http.router;
import core.time;
import vibe.core.core;
import vibe.core.log;
import vibe.data.json;
import vibe.http.client;
import std.datetime.systime;
import std.file;
import std.net.curl;
import std.json;
import std.ascii : LetterCase;
import std.digest;
import std.digest.md;
import std.digest.sha;
import std.string : representation;
import std.digest.hmac;
import vibe.data.serialization;
import mongodb;

Database db;

void main()
{
	db = new Database("reminders", "", "");

	auto router = new URLRouter;
	router
		.get("/checkReminders", &checkReminders)
		.post("/createReminder",&createReminder)
		.post("/changeReminder",&changeReminder);
	auto l = listenHTTP("0.0.0.0:6534", router);
	runApplication();

	scope (exit) {
	    exitEventLoop(true);
	    sleep(1.msecs);
    }

    l.stopListening();
}

void createReminder(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	db["workspace_reminders"].remove("text", "");
	Json reminder = Json.emptyObject;
	long period = 60 * 60 * 24 * req_end.json["period"].to!long;

	auto all_reminders = db["workspace_reminders"].find("", "");
	for(int i = 0; i < all_reminders.length; i++)
	{
		if(all_reminders[i]["bot_id"].to!string == req_end.json["bot_id"].to!string && all_reminders[i]["user_id"].to!string == req_end.json["user_id"].to!string)
			db["workspace_reminders"].remove("reminder_id", all_reminders[i]["reminder_id"].to!string);
	}

	reminder["text"] = req_end.json["text"].to!string.replace(" ", "%20");
	reminder["reminder_id"] = randomUUID().toString();
	reminder["time"] = Clock.currTime().toUnixTime() + period;
	reminder["user_id"] = req_end.json["user_id"].to!string;
	reminder["bot_id"] = req_end.json["bot_id"].to!string;
	reminder["block_id"] = req_end.json["block_id"].to!string;
	reminder["chatfuel_token"]  = req_end.json["chatfuel_token"].to!string;
	reminder["remind"] = "on";
	reminder["period"] = period;
	db["workspace_reminders"].insert(reminder);
	res_end.writeJsonBody(["status": "ok"]);
}

void changeReminder(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	if(req_end.json["remind"].to!string == "on")
	{
		db["workspace_reminders"].update("reminder_id", req_end.json["reminder_id"].to!string, "remind", "off");
		res_end.writeJsonBody(["set_attributes": ["reminder_subscribed": "off"]]);
	}
	if(req_end.json["remind"].to!string == "off")
	{
		db["workspace_reminders"].update("reminder_id", req_end.json["reminder_id"].to!string, "remind", "on");
		res_end.writeJsonBody(["set_attributes": ["reminder_subscribed": "on"]]);
	}
}

void checkReminders(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	long time = Clock.currTime().toUnixTime(), reminded = 0;
	auto reminders = db["workspace_reminders"].find("", "");
	for(int i = 0; i < reminders.length; i++)
		if(time > reminders[i]["time"].to!long && reminders[i]["remind"].to!string == "on")
		{
			requestHTTP("https://api.chatfuel.com/bots/" ~ reminders[i]["bot_id"].to!string ~ "/users/" ~ reminders[i]["user_id"].to!string ~ "/send?chatfuel_token=" ~ reminders[i]["chatfuel_token"].to!string ~ "&chatfuel_block_id=" ~ reminders[i]["block_id"].to!string ~ "&reminder_text=" ~ reminders[i]["text"].to!string ~ "&reminder_id=" ~ reminders[i]["reminder_id"].to!string ~ "&reminder_subscribed=on",					
				(scope req) {
					req.method = HTTPMethod.POST;
				},
				(scope res){
				}
			);
			db["workspace_reminders"].update("reminder_id", reminders[i]["reminder_id"].to!string, "time", (time + reminders[i]["period"].to!long).to!string);
			reminded++;
		}
	res_end.writeJsonBody(["reminded": reminded]);
}


